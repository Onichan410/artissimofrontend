using UnityEngine;
using UniRx;

public class ScrollData
{
    public FloatReactiveProperty ScrollDelta = new FloatReactiveProperty();
}
