using UnityEngine;
using UniRx;

public class SwipeData
{
    public enum MouseButton
    {
        None,
        Left,
        Middle,
        Right
    }
    public enum Direction
    {
        None,
        Down,
        Up,
        Left,
        Right
    }
    public enum Phase
    {
        None,
        Began,
        Moved,
        Ended
    }

    public GameObject ObjOnStart;
    public MouseButton Button;
    public Direction Dir;
    public ReactiveProperty<Phase> ClickPhase = new ReactiveProperty<Phase>();
    public Vector2 StartPosition;
    public Vector2 CurrentPosition;
    public Vector2 EndPosition;

    public void Info()
    {
        Debug.Log($"ObjOnStart - {ObjOnStart} \n" +
                  $"Button - {Button} \n" +
                  $"Direction - {Dir} \n" +
                  $"Start Position - {StartPosition} \n" + 
                  $"End Position - {EndPosition} \n");
    }
}
