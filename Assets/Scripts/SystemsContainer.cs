using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public static class SystemsContainer
{
    private static List<BaseSystem> _systems = new List<BaseSystem>();
    private static List<IExecuteStart> _executeStarts = new List<IExecuteStart>();
    private static List<IExecuteUpdate> _executeUpdates = new List<IExecuteUpdate>();
    public static void CreateSystems(AppData data)
    {
        _systems = new List<BaseSystem>()
        {
            new CameraSystem(data),
            new SwipeSystem(data),
            new ScrollSystem(data)
        };
        SortExecutes();
    }

    private static void SortExecutes()
    {
        foreach (var system in _systems)
        {
            List<Type> interfaces = system.GetType().GetInterfaces().ToList();
            if (interfaces.Contains(typeof(IExecuteStart)))
            {
                _executeStarts.Add((IExecuteStart) system);
            }
            if (interfaces.Contains(typeof(IExecuteUpdate)))
            {
                _executeUpdates.Add((IExecuteUpdate) system);
            }
        }
    }

    public static void DirectStart()
    {
        foreach (var system in _executeStarts)
        {
            system.Start();
        }
    }

    public static void DirectUpdate()
    {
        foreach (var system in _executeUpdates)
        {
            system.Update();
        }
    }
}
