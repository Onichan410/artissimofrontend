using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataContainer
{
    public static AppData CreateData()
    {
        AppData Data = new AppData
        {
            Config = Resources.Load<Config>("Config")
        };
        return Data;
    }
}
