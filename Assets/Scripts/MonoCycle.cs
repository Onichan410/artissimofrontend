using UnityEngine;

public class MonoCycle : MonoBehaviour
{
    private void Start()
    {
        SystemsContainer.DirectStart();
    }

    private void Update()
    {
        SystemsContainer.DirectUpdate();
    }
}
