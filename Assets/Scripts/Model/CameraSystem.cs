using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class CameraSystem : BaseSystem, IExecuteUpdate
{
    private CameraView _view;
    private Vector3 _lastPosition;
    private Vector3 _lastRotation;
    

    private const float DRAG_COMPENSATION = 100;
    
    private const float ZOOM_MULTIPLIER = 2;
    private const float MAX_ZOOM = 150;
    private const float MIN_ZOOM = 10;

    private const float ROTATE_COMPENSATION = 20;
    protected override void Init(AppData data)
    {
        base.Init(data);
        _view = GameObject.FindObjectOfType<CameraView>();
    }

    public void Update()
    {
        CheckAndDrag();
        CheckAndZoom();
        //CheckAndRotate();
    }
    
    private void CheckAndDrag()
    {
        if (_data.SwipeData.Button != SwipeData.MouseButton.Right)
        {
            return;
        }
        
        if (_data.SwipeData.ClickPhase.Value == SwipeData.Phase.Moved)
        {
            Vector3 result = _data.SwipeData.StartPosition - _data.SwipeData.CurrentPosition;
            _view.transform.localPosition = _lastPosition + result / DRAG_COMPENSATION;
        }

        if (_data.SwipeData.ClickPhase.Value == SwipeData.Phase.Ended)
        {
            _lastPosition = _view.transform.localPosition;
        }
    }

    private void CheckAndZoom()
    {
        if (_data.ScrollData.ScrollDelta.Value != 0)
        {
            _view.Camera.fieldOfView =
                Mathf.Clamp(_view.Camera.fieldOfView - _data.ScrollData.ScrollDelta.Value * ZOOM_MULTIPLIER, 
                    MIN_ZOOM, MAX_ZOOM);
        }
    }

    private void CheckAndRotate()
    {
        if (_data.SwipeData.Button != SwipeData.MouseButton.Middle)
        {
            return;
        }
        
        if (_data.SwipeData.ClickPhase.Value == SwipeData.Phase.Moved)
        {
            Vector3 result = _data.SwipeData.StartPosition - _data.SwipeData.CurrentPosition;
            _view.Camera.transform.localRotation = Quaternion.Euler(_lastRotation +
                                                             new Vector3(-result.y, result.x, 0)
                                                             / ROTATE_COMPENSATION);
        }

        if (_data.SwipeData.ClickPhase.Value == SwipeData.Phase.Ended)
        {
            _lastRotation = _view.Camera.transform.localEulerAngles;
        }
    }
    
    public CameraSystem(AppData data) : base(data)
    {
    }
}

