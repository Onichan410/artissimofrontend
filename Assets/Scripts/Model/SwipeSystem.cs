using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeSystem : BaseSystem, IExecuteUpdate
{
    private EventSystem _eventSystem;
    
    private Vector2 _startPos;
    private Vector2 _endPos;
    private SwipeData.MouseButton _button;
    private GameObject _objOnStart;
    private bool _isProcess;

    protected override void Init(AppData data)
    {
        base.Init(data);
        _isProcess = false;
        _eventSystem = GameObject.FindObjectOfType<EventSystem>();
    }

    public void Update()
    {
        if (!_isProcess)
        {
            _button = CheckButtonDown();
        }

        if (_button != SwipeData.MouseButton.None)
        {
            if (CheckButtonDown() == _button)
            {
                _isProcess = true;
                _startPos = Input.mousePosition;
                _objOnStart = _eventSystem.currentSelectedGameObject;
                _data.SwipeData.ClickPhase.Value = SwipeData.Phase.Began;
                CreateSwipe();
            }
            if (CheckButton() == _button)
            {
                _data.SwipeData.ClickPhase.Value = SwipeData.Phase.Moved;
                _data.SwipeData.CurrentPosition = Input.mousePosition;
            }
            if (CheckButtonUp() == _button)
            {
                _data.SwipeData.ClickPhase.Value = SwipeData.Phase.Ended;
                _isProcess = false;
                _endPos = Input.mousePosition;
                _data.SwipeData.EndPosition = _endPos;
                _data.SwipeData.Dir = GetDirection();
            }
        }
    }

    private void CreateSwipe()
    {
        _data.SwipeData = new SwipeData()
        {
            ObjOnStart = _objOnStart,
            StartPosition = _startPos,
            Button = _button
        };
    }

    private SwipeData.Direction GetDirection()
    {
        Vector2 result = _startPos - _endPos;
        if (Mathf.Abs(result.x) < Mathf.Abs(result.y))
        {
            return result.y < 0 ? SwipeData.Direction.Up : SwipeData.Direction.Down;
        }
        else
        {
            return result.x < 0 ? SwipeData.Direction.Right : SwipeData.Direction.Left;
        }
    }

    private SwipeData.MouseButton CheckButtonDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            return SwipeData.MouseButton.Left;
        }
        if (Input.GetMouseButtonDown(2))
        {
            return SwipeData.MouseButton.Middle;
        }
        if (Input.GetMouseButtonDown(1))
        {
            return SwipeData.MouseButton.Right;
        }

        return SwipeData.MouseButton.None;
    }
    private SwipeData.MouseButton CheckButton()
    {
        if (Input.GetMouseButton(0))
        {
            return SwipeData.MouseButton.Left;
        }
        if (Input.GetMouseButton(2))
        {
            return SwipeData.MouseButton.Middle;
        }
        if (Input.GetMouseButton(1))
        {
            return SwipeData.MouseButton.Right;
        }

        return SwipeData.MouseButton.None;
    }
    private SwipeData.MouseButton CheckButtonUp()
    {
        if (Input.GetMouseButtonUp(0))
        {
            return SwipeData.MouseButton.Left;
        }
        if (Input.GetMouseButtonUp(2))
        {
            return SwipeData.MouseButton.Middle;
        }
        if (Input.GetMouseButtonUp(1))
        {
            return SwipeData.MouseButton.Right;
        }

        return SwipeData.MouseButton.None;
    }

    public SwipeSystem(AppData data) : base(data)
    {
    }
}
