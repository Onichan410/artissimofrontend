
public abstract class BaseSystem
{
    internal AppData _data;
    protected virtual void Init(AppData data)
    {
        _data = data;
    }

    public BaseSystem(AppData data)
    {
        Init(data);
    }
}
