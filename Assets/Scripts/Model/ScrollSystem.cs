using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollSystem : BaseSystem, IExecuteUpdate
{
    public void Update()
    {
        _data.ScrollData.ScrollDelta.Value = Input.mouseScrollDelta.y;
    }
    
        
    public ScrollSystem(AppData data) : base(data)
    {
    }
}
