using UnityEngine;

public class MonoInitializer : MonoBehaviour
{
    private void Awake()
    {
        AppData data = DataContainer.CreateData();
        SystemsContainer.CreateSystems(data);
    }
}
